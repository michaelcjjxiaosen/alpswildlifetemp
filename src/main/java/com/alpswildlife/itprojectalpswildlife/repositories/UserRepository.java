package com.alpswildlife.itprojectalpswildlife.repositories;

import com.alpswildlife.itprojectalpswildlife.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    /**
     * The method is used for registering a account.
     * @param s
     * @param <S>
     * @return
     */
    @Override
    <S extends User> S save(S s);

    /**
     * The method is used for login by finding the user.
     * @param name
     * @param password
     * @return
     */
    List<User> findByUsernameAndPassword(String name, String password);

    /**
     * The method is used for updating the user state.
     * @param state
     * @param id
     * @param name
     */
    @Modifying
    @Query(value = "update animalps_user set user_state = ?1 where user_id = ?2 and user_name = ?3", nativeQuery = true)
    void updateUserStateByIdAndName(Integer state, Integer id, String name);
}
