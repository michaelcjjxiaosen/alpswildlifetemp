package com.alpswildlife.itprojectalpswildlife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItProjectAlpsWildlifeApplication {
    public static void main(String[] args) {
        SpringApplication.run(ItProjectAlpsWildlifeApplication.class, args);
    }
}
