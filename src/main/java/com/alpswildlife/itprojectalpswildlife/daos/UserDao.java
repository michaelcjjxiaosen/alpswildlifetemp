package com.alpswildlife.itprojectalpswildlife.daos;

import com.alpswildlife.itprojectalpswildlife.entities.User;
import com.alpswildlife.itprojectalpswildlife.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDao {
    @Autowired
    private UserRepository userRepository;

    public User saveAccountInfo(String username, String password, String type){
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setType(type);
        return userRepository.save(user);
    }

    public User findByUsernameAndPassword(String username, String password){
        //Here is a problem, the user id should be the unique attribute for identifying a user
        return this.userRepository.findByUsernameAndPassword(username, password).get(0);
    }

    public void updateUserStateByIdAndName(Integer state, Integer id, String username){
       this.userRepository.updateUserStateByIdAndName(state, id, username);
    }
}
