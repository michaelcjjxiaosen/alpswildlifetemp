CREATE TABLE animalps_user (
	UserId NUMERIC (10),
	UserName VARCHAR (40) NOT NULL,
	UserPassword VARCHAR (256) NOT NULL,
	UserState NUMERIC (1) NOT NULL,
	UserType VARCHAR (3) NOT NULL,
	PRIMARY KEY (UserId)
);

CREATE TABLE picture (
	PictureId NUMERIC (10),
	UserId NUMERIC (10) NOT NULL,
	PictureImage VARCHAR (512) NOT NULL,
	PictureDescription VARCHAR (512),
	PictureUploadDate date NOT NULL,
	PictureState NUMERIC (1) NOT NULL,
	PictureStateDate date NOT NULL,
	PictureRanking1 NUMERIC (10),
	PictureRanking2 NUMERIC (10),
	PictureRanking3 NUMERIC (10),
	PictureRanking4 NUMERIC (10),
	PictureRanking5 NUMERIC (10),
	PRIMARY KEY (PictureId),
	FOREIGN KEY (UserId) REFERENCES animalps_user(UserId)
);

CREATE TABLE likes (
	PictureId NUMERIC (10),
	UserId NUMERIC (10),
	PRIMARY KEY (PictureId, UserId),
	FOREIGN KEY (PictureId) REFERENCES Picture(PictureId),
	FOREIGN KEY (UserId) REFERENCES animalps_user(UserId)
);

CREATE TABLE PictureComment (
	CommentId NUMERIC (10),
	PictureId NUMERIC (10) NOT NULL,
	UserId NUMERIC (10) NOT NULL,
	CommentDescription VARCHAR (512) NOT NULL,
	CommentDate date NOT NULL,
	PRIMARY KEY (CommentId),
	FOREIGN KEY (PictureId) REFERENCES Picture(PictureId),
	FOREIGN KEY (UserId) REFERENCES animalps_user(UserId)
);

CREATE TABLE category (
	CategoryId NUMERIC (4),
	CategoryName VARCHAR (40) NOT NULL,
	PRIMARY KEY (CategoryId)
);

CREATE TABLE tag (
	TagId NUMERIC (10),
	CategoryId NUMERIC (4) NOT NULL,
	TagName VARCHAR (40) NOT NULL,
	TagDescription VARCHAR (512),
	primary key (TagId),
	foreign key (CategoryId) references Category(CategoryId)
);

create table picturetags (
	PictureId numeric (10),
	TagId numeric (10),
	primary key (PictureId, TagId),
	foreign key (PictureId) references Picture(PictureId),
	foreign key (TagId) references Tag(TagId)
);
