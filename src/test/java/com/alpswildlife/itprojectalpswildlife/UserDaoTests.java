package com.alpswildlife.itprojectalpswildlife;

import com.alpswildlife.itprojectalpswildlife.daos.UserDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import javax.transaction.Transactional;

@SpringBootTest
public class UserDaoTests {

    @Autowired
    private UserDao userDao;

    @Test
    public void testSaveAccountInfo(){
        Assertions.assertNotEquals(null, this.userDao.saveAccountInfo("Michael", "123456", "professional"));
    }

    @Test
    public void testFindByNameAndPassword(){
        Assertions.assertNotEquals(null, this.userDao.findByUsernameAndPassword("Michael", "123456"));
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testUpdateUserStateByIdAndName(){
        try {
            this.userDao.updateUserStateByIdAndName(1, 1, "Steven");
        }catch (Exception e){
            Assertions.fail();
        }
    }

}
